<?php

namespace App\Controllers;

use CodeIgniter\RESTful\ResourceController;
use CodeIgniter\API\ResponseTrait;
use App\Models\ProductModel;

class Product extends ResourceController
{
    use ResponseTrait;
    protected $modelName = 'App\Models\ProductModel';
    protected $format    = 'json';
    // get all product
    public function index()
    {
        $model = new ProductModel();
        $data = $model->findAll();
        return $this->respond($data, 200);

        // $model = new ProductModel();
        // $result = $model->findAll();
        // $data = $model->asArray()->findAll();
        // return $this->respond($data, 200);
        // return $this->respond($data);
        // return view('privat/privat', $data);
        // $data = json_decode($result->getBody()->getContents(),true);
        // $this->load->view('privat/privat', $data);

        // $model = new ProductModel();
        // $data['product']=json_decode($this->curl->simple_get($this->api), true);
        // return $this->load->view('privat', $data);

        //cara lama(works):
        // $this->db      = \Config\Database::connect();
        // $builder = $this->db->table('product');
        // $query   = $builder->get()->getResult();  // Produces: SELECT * FROM mytable
        // $data['product'] = $query;
        // return view('privat/privat', $data);

        //cara sendiri
        // $model = new ProductModel();
        // $data = [
        //     'coba' => $model->findAll()
        // ];
        // // return $this->respond($data, 200);
        // return view('privat/privat', $data);
        // $view = 'privat';
        // return $this->$view;

        //https://cangkalinfo.blogspot.com/2017/02/codeigniter-mengambil-data-json-dari-url.html
        // $url = "http://localhost:8080/product";
        // $get_url = file_get_contents($url);
        // $data = json_decode($get_url);

        // $data_array = array(
        //     'datalist' => $data
        // );
        // $this->load->view('json/json_list', $data_array);
    }

    // get single product
    public function show($id = null)
    {
        $model = new ProductModel();
        $data = $model->getWhere(['product_id' => $id])->getResult();
        if ($data) {
            return $this->respond($data);
        } else {
            return $this->failNotFound('No Data Found with id ' . $id);
        }
    }

    // create a product
    public function create()
    {
        $model = new ProductModel();
        $data = [
            'product_name' => $this->request->getPost('product_name'),
            'product_price' => $this->request->getPost('product_price')
        ];
        $data = json_decode(file_get_contents("php://input"));
        //$data = $this->request->getPost();
        $model->insert($data);
        $response = [
            'status'   => 201,
            'error'    => null,
            'messages' => [
                'success' => 'Data Saved'
            ]
        ];

        return $this->respondCreated($data, 201);
    }

    // update product
    public function update($id = null)
    {
        $model = new ProductModel();
        $json = $this->request->getJSON();
        if ($json) {
            $data = [
                'product_name' => $json->product_name,
                'product_price' => $json->product_price
            ];
        } else {
            $input = $this->request->getRawInput();
            $data = [
                'product_name' => $input['product_name'],
                'product_price' => $input['product_price']
            ];
        }
        // Insert to Database
        $model->update($id, $data);
        $response = [
            'status'   => 200,
            'error'    => null,
            'messages' => [
                'success' => 'Data Updated'
            ]
        ];
        return $this->respond($response);
    }

    // delete product
    public function delete($id = null)
    {
        $model = new ProductModel();
        $data = $model->find($id);
        if ($data) {
            $model->delete($id);
            $response = [
                'status'   => 200,
                'error'    => null,
                'messages' => [
                    'success' => 'Data Deleted'
                ]
            ];

            return $this->respondDeleted($response);
        } else {
            return $this->failNotFound('No Data Found with id ' . $id);
        }
    }
}
