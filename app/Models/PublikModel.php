<?php

namespace App\Models;

use CodeIgniter\Model;

class PublikModel extends Model
{
    protected $table = 'publik';
    protected $primaryKey = 'id';
    protected $allowedFields = ['nama', 'jeniskelamin', 'asaldaerah', 'status'];
}
