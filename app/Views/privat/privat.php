<?= $this->extend('pages/templateatas'); ?>

<?= $this->section('content'); ?>
<div class="card">
    <div class="card-header">
        <h4>Data Product</h4>
    </div>
    <div class="card-body">
        <div class="section-title mt-0"></div>
        <table class="table table-hover">
            <thead>
                <tr>
                    <th scope="col">No</th>
                    <th scope="col">Nama Produk</th>
                    <th scope="col">Harga</th>
                </tr>
            </thead>
            <tbody>            
            <?php foreach ($data as $key => $value) : ?>
                    <tr>
                        <th scope="row">1</th>
                        <td><?= $value->product_name ?></td>
                        <td><?= $value->product_price ?></td>
                    </tr>
                <?php endforeach; ?>
                <tr>
                    <th scope="row">3</th>
                    <td>Larry</td>
                    <td>the Bird</td>
                </tr>
            </tbody>
        </table>
        <?= $this->endSection(); ?>