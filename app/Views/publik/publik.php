<?= $this->extend('pages/templateatas'); ?>

<?= $this->section('content'); ?>
<section class="section">
    <div class="section-header">
        <h1>Data Publik</h1>
        <div class="section-header-breadcrumb">
            <div class="breadcrumb-item active"><a href="/publik/new">Tambah Data</a></div>
            <div class="breadcrumb-item"><a href="#">Layout</a></div>
        </div>
    </div>
    <div class="card">
        <div class="card-header">
            <h4>
                <?php if (session()->getFlashdata('pesan')) : ?>
                    <div class="alert alert-success" role="alert">
                        <?= session()->getFlashdata('pesan'); ?>
                    </div>
                <?php endif; ?>                
            </h4>

        </div>
        <div class="card-body">
            <div class="section-title mt-0"></div>
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th scope="col">No</th>
                        <th scope="col">Nama</th>
                        <th scope="col">Jenis Kelamin</th>
                        <th scope="col">Asal Daerah</th>
                        <th scope="col">Status</th>
                        <th scope="col">Edit</th>
                        <th scope="col">Delete</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($publik as $key => $value) : ?>
                        <tr>
                            <th scope="row"><?= $key + 1 ?></th>
                            <td><?= $value->nama ?></td>
                            <td><?= $value->jeniskelamin ?></td>
                            <td><?= $value->asaldaerah ?></td>
                            <td><?= $value->status ?></td>
                            <td><a href="/publik/$1"><i class="fas fa-edit"></i></a></td>
                            <td><a href="/publik/$1"><i class="fas fa-trash-alt badge-danger"></i></a></td>
                        </tr>
                    <?php endforeach; ?>
                    <tr>
                        <th scope="row">3</th>
                        <td>Larry</td>
                        <td>the Bird</td>
                        <td>@twitter</td>
                    </tr>
                </tbody>
            </table>
</section>
            <?= $this->endSection(); ?>