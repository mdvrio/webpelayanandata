<?= $this->extend('pages/templateatas'); ?>

<?= $this->section('content'); ?>
<section class="section">
    <div class="container">
        <div class="row">
            <div class="col-8">
                <h2 class="my-3">Form Tambah Data Publik</h2>
                <!-- $validation->listErrors(); -->
                <!-- <form action="/publik" method="post" enctype="multipart/form-data"> -->
                <form action="/publik" method="POST">
                    <?= csrf_field(); ?>
                    <div class="row mb-3">
                        <label for="nama" class="col-sm-2 col-form-label">Nama</label>
                        <div class="col-sm-5">
                            <input type="text" class="form-control <?= ($validation->hasError('nama')) ? 'is-invalid' : ''; ?>" id="nama" name="nama">
                            <div class="invalid-feedback">
                                <?= $validation->getError('nama'); ?>.
                            </div>
                        </div>
                    </div>
                    <div>
                        <label for="jeniskelamin" class="space">Jenis Kelamin</label>
                          <input class="ml-4" type="radio" id="Laki-Laki" name="jeniskelamin" value="Laki-Laki">
                          <label for="Laki-Laki">Laki-Laki</label>
                          <input type="radio" id="Perempuan" name="jeniskelamin" value="Perempuan">
                          <label for="Perempuan">Perempuan</label><br>
                    </div>
                    <div class="row mb-3">
                        <label for="asaldareah" class="col-sm-2 col-form-label">Asal Daerah</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="asaldaerah" name="asaldaerah" value="<?= old('asaldaerah'); ?>">
                        </div>
                    </div>
                    <div class="row mb-3">
                        <label for="status" class="col-sm-2 col-form-label">Status</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="status" name="status" value="<?= old('status'); ?>">
                        </div>
                    </div>
                    <!-- <div class="row mb-3">
                    <label for="sampul" class="col-sm-2 col-form-label">Sampul</label>
                    <div class="col-sm-2">
                        <img src="/img/default.png" class="img-thumbnail img-preview">
                    </div>
                    <div class="col-sm-8">
                        <div class="custom-file">
                            <input type="file" class="custom-file-input phpp ($validation->hasError('sampul')) ? 'is-invalid' : ''; ?>" id="sampul" name="sampul" onchange="previewImg()">
                            <div class="invalid-feedback">
                                phpp $validation->getError('sampul'); ?>.
                            </div>
                            <label class="custom-file-label" for="sampul">Pilih Gambar Sampul</label>
                        </div>

                    </div>
                </div> -->
                    <!-- <div class="row mb-3">
                    <div class="col-sm-10 offset-sm-2">
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" id="gridCheck1">
                            <label class="form-check-label" for="gridCheck1">
                                Example checkbox
                            </label>
                        </div>
                    </div>
                </div> -->
                    <div class="invalid-feedback alert alert-danger alert-dismissible show fade">
                        <div class="alert-body">
                            <button class="close" data-dismiss="alert">
                                <span>&times;</span>
                            </button>
                            <?= $validation->getError('nama'); ?>
                        </div>
                    </div>
                    <?php if (!empty(session()->getFlashdata('error'))) : ?>
                    <div class="alert alert-danger alert-dismissible fade show" role="alert">
                        <h4>Periksa Form Kembali</h4>
                        </hr />
                        <?php echo session()->getFlashdata('error'); ?>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                <?php endif; ?>
                    <button type="submit" class="btn btn-primary">Tambah Data</button>
                </form>                
            </div>
        </div>
    </div>
</section>
<?= $this->endSection(); ?>